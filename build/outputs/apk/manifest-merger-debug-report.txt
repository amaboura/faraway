-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	package
		ADDED from AndroidManifest.xml:3:5
	android:versionName
		ADDED from AndroidManifest.xml:5:5
	android:versionCode
		ADDED from AndroidManifest.xml:4:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
uses-sdk
ADDED from AndroidManifest.xml:7:5
MERGED from com.facebook.android:facebook-android-sdk:3.21.1:20:5
MERGED from com.android.support:support-v4:21.0.0:15:5
MERGED from ParseAndroidUI:ParseLoginUI:unspecified:7:5
MERGED from com.android.support:support-v4:21.0.0:15:5
MERGED from com.android.support:support-v4:21.0.0:15:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:9:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:8:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:12:5
	android:name
		ADDED from AndroidManifest.xml:12:22
android:uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:14:5
	android:maxSdkVersion
		ADDED from AndroidManifest.xml:16:9
	android:name
		ADDED from AndroidManifest.xml:15:9
android:uses-permission#android.permission.READ_PHONE_STATE
ADDED from AndroidManifest.xml:17:5
	android:name
		ADDED from AndroidManifest.xml:17:30
android:uses-permission#android.permission.READ_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:18:5
	android:maxSdkVersion
		ADDED from AndroidManifest.xml:20:9
	android:name
		ADDED from AndroidManifest.xml:19:9
application
ADDED from AndroidManifest.xml:22:5
MERGED from com.facebook.android:facebook-android-sdk:3.21.1:24:5
MERGED from com.android.support:support-v4:21.0.0:16:5
MERGED from ParseAndroidUI:ParseLoginUI:unspecified:11:5
MERGED from com.android.support:support-v4:21.0.0:16:5
MERGED from com.android.support:support-v4:21.0.0:16:5
	android:label
		ADDED from AndroidManifest.xml:26:9
	android:allowBackup
		ADDED from AndroidManifest.xml:24:9
	android:icon
		ADDED from AndroidManifest.xml:25:9
	android:theme
		ADDED from AndroidManifest.xml:27:9
	android:name
		ADDED from AndroidManifest.xml:23:9
activity#com.aboura.aljazeera.ProfileActivity
ADDED from AndroidManifest.xml:28:9
	android:label
		ADDED from AndroidManifest.xml:30:13
	android:launchMode
		ADDED from AndroidManifest.xml:31:13
	android:name
		ADDED from AndroidManifest.xml:29:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:32:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:33:17
	android:name
		ADDED from AndroidManifest.xml:33:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:35:17
	android:name
		ADDED from AndroidManifest.xml:35:27
activity#com.parse.ui.ParseLoginActivity
ADDED from AndroidManifest.xml:38:9
	android:label
		ADDED from AndroidManifest.xml:40:13
	android:launchMode
		ADDED from AndroidManifest.xml:41:13
	android:name
		ADDED from AndroidManifest.xml:39:13
meta-data#com.parse.ui.ParseLoginActivity.PARSE_LOGIN_ENABLED
ADDED from AndroidManifest.xml:42:13
	android:value
		ADDED from AndroidManifest.xml:44:17
	android:name
		ADDED from AndroidManifest.xml:43:17
meta-data#com.parse.ui.ParseLoginActivity.PARSE_LOGIN_EMAIL_AS_USERNAME
ADDED from AndroidManifest.xml:45:13
	android:value
		ADDED from AndroidManifest.xml:47:17
	android:name
		ADDED from AndroidManifest.xml:46:17
meta-data#com.parse.ui.ParseLoginActivity.FACEBOOK_LOGIN_ENABLED
ADDED from AndroidManifest.xml:48:13
	android:value
		ADDED from AndroidManifest.xml:50:17
	android:name
		ADDED from AndroidManifest.xml:49:17
meta-data#com.parse.ui.ParseLoginActivity.TWITTER_LOGIN_ENABLED
ADDED from AndroidManifest.xml:51:13
	android:value
		ADDED from AndroidManifest.xml:53:17
	android:name
		ADDED from AndroidManifest.xml:52:17
activity#com.facebook.LoginActivity
ADDED from AndroidManifest.xml:55:9
	android:label
		ADDED from AndroidManifest.xml:57:13
	android:launchMode
		ADDED from AndroidManifest.xml:58:13
	android:name
		ADDED from AndroidManifest.xml:56:13
meta-data#com.facebook.sdk.ApplicationId
ADDED from AndroidManifest.xml:60:9
	android:value
		ADDED from AndroidManifest.xml:62:13
	android:name
		ADDED from AndroidManifest.xml:61:13
activity#com.aboura.aljazeera.FriendsActivity
ADDED from AndroidManifest.xml:64:9
	android:label
		ADDED from AndroidManifest.xml:66:13
	android:name
		ADDED from AndroidManifest.xml:65:13
activity#com.aboura.aljazeera.DistanceActivity
ADDED from AndroidManifest.xml:68:9
	android:label
		ADDED from AndroidManifest.xml:70:13
	android:name
		ADDED from AndroidManifest.xml:69:13
activity#com.aboura.aljazeera.DetailsActivity
ADDED from AndroidManifest.xml:72:9
	android:label
		ADDED from AndroidManifest.xml:74:13
	android:name
		ADDED from AndroidManifest.xml:73:13
activity#com.aboura.aljazeera.MapActivity
ADDED from AndroidManifest.xml:76:9
	android:label
		ADDED from AndroidManifest.xml:78:13
	android:name
		ADDED from AndroidManifest.xml:77:13
