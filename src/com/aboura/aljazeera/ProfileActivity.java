package com.aboura.aljazeera;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.model.GraphUser;
import com.parse.ParseUser;
import com.parse.ui.ParseLoginBuilder;
import com.parse.ui.ParseLoginFragment;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ProfileActivity extends Activity {
  private static final int LOGIN_REQUEST = 0;

  private TextView titleTextView;
  private TextView emailTextView;
  private TextView nameTextView;
  private Button loginOrLogoutButton;
  private Button btnShowFriendsList;
  public static List<FacebookUser> friendsList;
  public static List<String> friendNameList;
  public static String currentLocation;
  private static int friendsCount;

  private ParseUser currentUser;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_profile);
    titleTextView = (TextView) findViewById(R.id.profile_title);
    emailTextView = (TextView) findViewById(R.id.profile_email);
    nameTextView = (TextView) findViewById(R.id.profile_name);
    loginOrLogoutButton = (Button) findViewById(R.id.login_or_logout_button);
    titleTextView.setText(R.string.profile_title_logged_in);
    btnShowFriendsList = (Button) findViewById(R.id.btnShowFriendsList);
    btnShowFriendsList.setEnabled(false);
    friendsList = new LinkedList<FacebookUser>();
    friendNameList = new LinkedList<String>();
    btnShowFriendsList.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showFriendsList();
        }
    });


    loginOrLogoutButton.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (currentUser != null) {
                // User clicked to log out.
                ParseUser.logOut();
                currentUser = null;
                showProfileLoggedOut();
            } else {
                // User clicked to log in.
                ParseLoginBuilder loginBuilder = new ParseLoginBuilder(
                        ProfileActivity.this);
                String[] permissions = {"email", "user_location", "user_friends",
                                        "user_about_me","user_relationships","public_profile"};
                List<String> list = Arrays.asList(permissions);
                loginBuilder.setFacebookLoginPermissions(list);
                startActivityForResult(loginBuilder.build(), LOGIN_REQUEST);
            }
        }
    });
  }


  @Override
  protected void onStart() {
    super.onStart();

    currentUser = ParseUser.getCurrentUser();
    if (currentUser != null) {
      showProfileLoggedIn();


    } else {
      showProfileLoggedOut();
    }
  }

  /**
   * Shows the profile of the given user.
   */
  private void showProfileLoggedIn() {
    titleTextView.setText(R.string.profile_title_logged_in);
    emailTextView.setText(currentUser.getEmail());
    String fullName = currentUser.getString("name");
      try {
          JSONObject myLocation = currentUser.getJSONObject("location");
          if (myLocation != null) {
              currentLocation = myLocation.getString("name");
          }
          else{
              currentLocation = "Doha";
          }
      }
      catch(JSONException error){

      }


    if (fullName != null) {

      nameTextView.setText(fullName);

    }
    loginOrLogoutButton.setText(R.string.profile_logout_button_label);
    btnShowFriendsList.setEnabled(true);
  }

  // show Friends list
  private void showFriendsList()  {

      Request request = Request.newMyFriendsRequest(ParseLoginFragment.currentSession, new Request.GraphUserListCallback() {

          @Override
          public void onCompleted(List<GraphUser> users, Response response) {

              if (users != null) {
                  List<String> friendsLocations = new ArrayList<String>();
                  Log.d("Size", Integer.toString(users.size()));
                  friendsCount = users.size();
                  for (GraphUser user : users) {
                      Log.d("Name",user.getName());
                      Request locationRequest = Request.newGraphPathRequest(ParseLoginFragment.currentSession, '/' + user.getId(), new Request.Callback() {
                          @Override
                          public void onCompleted(Response response) {
                              try{
                                  JSONObject jsonObject = new JSONObject(response.getRawResponse());
                                  String friendLocation = jsonObject.getJSONObject("location").getString("name");
                                  String friendName = jsonObject.getString("name");
                                  // add friend to friendList
                                  FacebookUser friend = new FacebookUser(friendName,friendLocation);
                                  friendsList.add(friend);
                                  friendNameList.add(friendName);

                                  if (friendsList.size() == friendsCount){
                                      Intent intent = new Intent(getApplicationContext(),FriendsActivity.class);
                                      startActivity(intent);
                                  }
                              }
                              catch(JSONException err){
                                  Log.d("JsonError",err.getMessage());
                              }

                          }
                      });

                      locationRequest.executeAsync();
                  }
              } else {
                  Log.d("FB", "No friend list");
              }

          }


      });

      request.executeAsync();



  }

  /**
   * Show a message asking the user to log in, toggle login/logout button text.
   */
  private void showProfileLoggedOut() {
    titleTextView.setText(R.string.profile_title_logged_out);
    emailTextView.setText("");
    nameTextView.setText("");
    loginOrLogoutButton.setText(R.string.profile_login_button_label);
  }
}
