package com.aboura.aljazeera;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class DetailsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("distance");
            final String mapUrl = extras.getString("mapUrl");
            int position = (int)extras.get("position");
            Log.d("Position",extras.get("position").toString());
            TextView txtDistance = (TextView) findViewById(R.id.txtDistance);
            TextView txtFriendName = (TextView) findViewById(R.id.txtFriendName);
            Log.d("Distance Details:",value);
            txtDistance.setText(value);
            txtFriendName.setText(ProfileActivity.friendNameList.get(position));

            // show map button

            Button btnShowMap = (Button) findViewById(R.id.btnShowMap);
            btnShowMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent showMap = new Intent(getApplicationContext(),MapActivity.class);
                    showMap.putExtra("mapUrl",mapUrl);
                    startActivity(showMap);
                }
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
