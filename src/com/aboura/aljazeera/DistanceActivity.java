package com.aboura.aljazeera;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.squareup.okhttp.*;

import java.io.IOException;

public class DistanceActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance);

        // return result from Wolfram cloud
        final String ViewDistanceId = "0df12ce5-8af2-49b2-a906-97df1b2e41c1"; // Request Id for GeoDistance
        final String ViewMapId = "d3c8da01-5103-40f0-b016-bbf3b5857926"; // Request
        final String CityOne = ProfileActivity.currentLocation.replaceAll("\\s","%20").replace(',','+');
        final String CityTwo;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            CityTwo = ((FacebookUser) ProfileActivity.friendsList.get((int) extras.get("position"))).Location.replaceAll("\\s","%20").replace(',','+');
            try {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            GetWolframResult(GetWolframUrl(ViewDistanceId, CityOne, CityTwo),
                                    GetWolframUrl(ViewMapId, CityOne, CityTwo));

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                thread.start();

            } catch (Exception ex) {

            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_distance, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void GetWolframResult(String distanceUrl, String mapUrl) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(distanceUrl)
                .header("User-Agent", "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0")
                .build();
        Log.d("Request Url: ", distanceUrl);
        Response response = null;
        try {
            response = client.newCall(request).execute();
            Intent showDetails = new Intent(getApplicationContext(),DetailsActivity.class);
            String fullDistance = response.body().string();
            String unit = fullDistance.substring(9).split(",")[1].replace(']',' ').replace('"',' ');
            showDetails.putExtra("distance",fullDistance.substring(9).split(",")[0].substring(0,9) + " " + unit);
            showDetails.putExtra("mapUrl", mapUrl);
            Bundle extras = getIntent().getExtras();
            showDetails.putExtra("position", (int)extras.get("position"));
            startActivity(showDetails);

        }
        catch(IOException err){
            Log.d("IOException :", err.getMessage());
        }
    }

    public String GetWolframUrl(String ObjectId, String CityOne, String CityTwo){
        return "https://www.wolframcloud.com/objects/" + ObjectId + "?city1=" + CityOne + "&city2=" + CityTwo;
    }

}
