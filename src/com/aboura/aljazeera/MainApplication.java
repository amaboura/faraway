package com.aboura.aljazeera;

import android.app.Application;

import com.parse.Parse;


public class MainApplication extends Application {
  @Override
  public void onCreate() {
    super.onCreate();

    Parse.initialize(this, getString(R.string.parse_app_id),
        getString(R.string.parse_client_key));

    Parse.setLogLevel(Parse.LOG_LEVEL_DEBUG);

  }
}
